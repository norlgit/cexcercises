#include <stdio.h>

void vegas(void);

int main()
{
    int a;

    a = 365;
    printf("In der Funktion main, a=%d\n",a);
    vegas();
    printf("In der Funktion main, a=%d\n",a);
    return 0;
}

void vegas(void)
{
    int a;

    a = -10;
    printf("In der Funktion vegas, a=%d\n",a);
}
